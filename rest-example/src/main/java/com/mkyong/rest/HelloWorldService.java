package com.mkyong.rest;
 
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
 
@Path("/hello")
public class HelloWorldService {
 
	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String msg) {
		String output = "Jersey say : " + msg;
		System.out.println("In branch 1. adding this print line statement");
		needsToGetIntoMasterAfterRebase();
		postTagCommit();
		bugFix1();
		bugFix2();
		newBranchChange1();
		newBranchChange2();
		noFfBranchChange1();
		noFfBranchChange2();
		return Response.status(200).entity(output).build();		
	}
	@GET
	@Path("/{param}")
	public Response getMsg1(@PathParam("param") String msg) {
		return null;
	}
	
	private void needsToGetIntoMasterAfterRebase(){
		System.out.println("masterMethod");		
	}
	
	private void postTagCommit(){
		System.out.println("postTagCommit");		
	}
	
	private void bugFix1(){
		System.out.println("bugFix1");		
	}
	
	private void bugFix2(){
		System.out.println("bugFix2");		
	}
	
	private void newBranchChange1(){
		System.out.println("newBranchChange1");		
	}
	
	private void newBranchChange2(){
		System.out.println("newBranchChange2");		
	}
	
	private void noFfBranchChange1(){
		System.out.println("noFfBranchChange1");		
	}
	
	private void noFfBranchChange2(){
		System.out.println("noFfBranchChange2");		
	}
	
	
}
